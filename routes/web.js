const UploadService = require("../services/uploadService");

const express = require('express');
const router = express.Router();

// The main route
router.get('/', (request, response) => {
  response.render('home', {
    title: 'Home',
    description: 'test'
  })
});

router.post('/fileupload', (req, res) => {
  UploadService.uploadImage(req, res);
});

module.exports = router;
