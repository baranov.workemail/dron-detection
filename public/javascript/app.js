import './bootstrap.js'
import 'jquery/src/jquery'


$(document).ready(function(){
    $("#photo").on('change', (e)=>{
        e.preventDefault();
        let myform = $("#uploadFile")[0];
        let data = new FormData(myform);
        //$('#loader1').show();

        $(document).ajaxStart(function(){
            $("#loader1").show();
        });

        $.ajax({
            url: "/fileupload",
            type: 'POST',
            data: data,
            async: false,
            beforeSend: function(){
                $("#loader1").show();
            },
            success: function (data) {
                console.log(data);
                let isDrone = data.is_drone ? "It's Drone" : "Not Drone";
                let image = "<div class='col-4 uploaded-img-container'><img class='uploaded-img' src='./images/upload/"+data.image_name+"'/><div class='drone-text'>"+isDrone+"</div></div>";
                $('.image-container').append(image);
                $('#loader1').hide();
            },
            cache: false,
            contentType: false,
            processData: false
        });

        return false;
    });
});


