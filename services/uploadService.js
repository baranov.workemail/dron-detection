const path = require('path');
const multer = require('multer');
const DroneDetector = require("../services/droneDetector");

class UploadService {
    static uploadImage(req, res){
        const storage = multer.diskStorage({
            destination: './public/images/upload',
            filename: (req, file, cb) => {
                let fname = file.fieldname + '-' + Date.now() + path.extname(file.originalname);
                cb(null, fname);
            }
        });

        const upload = multer({
            storage: storage,
            limits: { fileSize: 1000000 },
            fileFilter: (req, file, cb) => {
                this.checkFileType(file, cb);
            }
        }).single('photo');

        upload(req, res, async (err) => {
            if (err) {
                await res.json({ success: false, msg: err });
            }
            else {
                if (req.file === undefined) {
                    await res.json({ suceess: true, msg: 'No file selected' });
                } else {
                    let isDrone = await DroneDetector.isDrone(req.file.path);
                    return await res.json({ success: true, is_drone: isDrone, image_name: req.file.filename });
                }
            }
        });
    }

    static checkFileType(file, cb) {
        const filetypes = /jpeg|png|jpg|gif/;
        const extname = filetypes.test(path.extname(file.originalname).toLowerCase());
        const mimetype = filetypes.test(file.mimetype);
        if (mimetype && extname) {
            return cb(null, true);
        } else {
            cb('Error :Images only')
        }
    }
}

module.exports = UploadService;
